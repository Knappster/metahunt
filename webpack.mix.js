const mix = require('laravel-mix');
const webpack = require('webpack');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.webpackConfig({
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery'
			, jQuery: 'jquery'
			, jquery: 'jquery'
			, 'window.jQuery': 'jquery'
		})
	]
});

mix.js('resources/themes/admin/assets/js/app.js', 'public/assets/admin/js')
	.js('resources/themes/metahunt/assets/js/app.js', 'public/assets/js')
	.sass('resources/themes/admin/assets/sass/app.scss', 'public/assets/admin/css')
	.sass('resources/themes/metahunt/assets/sass/app.scss', 'public/assets/css')
	.sourceMaps();
