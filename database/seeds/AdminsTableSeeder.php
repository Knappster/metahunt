<?php

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Admin::create([
			'name' => 'Admin'
			, 'email' => 'admin@metahunt.net'
			, 'password' => Hash::make(env('ADMIN_PASS', 'password'))
		]);
	}
}
