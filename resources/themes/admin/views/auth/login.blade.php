<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>Metahunt</title>

	<!-- Fonts -->
	<link rel="dns-prefetch" href="//fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

	<!-- Styles -->
	<link href="{{ asset('assets/admin/css/app.css') }}" rel="stylesheet">

	<!-- Ionicons -->
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		Metahunt
	</div>
	<!-- /.login-logo -->
	<div class="card">
		<div class="card-body login-card-body">
			<p class="login-box-msg">Sign in to start your session</p>

			<form action="{{ route('admin.login') }}" method="post">
				@csrf

				<div class="input-group mb-3">
					<input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
						type="email"
						name="email"
						value="{{ old('email') }}"
						placeholder="Email"
						required
						autofocus>
					<div class="input-group-append">
						<span class="fa fa-envelope input-group-text"></span>
					</div>
					@if ($errors->has('email'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('email') }}</strong>
						</span>
					@endif
				</div>
				<div class="input-group mb-3">
					<input class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
						type="password"
						name="password"
						placeholder="Password"
						required>
					<div class="input-group-append">
						<span class="fa fa-lock input-group-text"></span>
					</div>
					@if ($errors->has('password'))
						<span class="invalid-feedback" role="alert">
							<strong>{{ $errors->first('password') }}</strong>
						</span>
					@endif
				</div>
				<div class="row">
					<div class="col-8">
						<div class="checkbox icheck">
							<label>
								<input type="checkbox "name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
							</label>
						</div>
					</div>
					<!-- /.col -->
					<div class="col-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
					</div>
					<!-- /.col -->
				</div>
			</form>

			@if (Route::has('password.request'))
			<p class="mb-1">
				<a href="{{ route('password.request') }}">{{ __('Forgot Your Password?') }}</a>
			</p>
			@endif
		</div>
		<!-- /.login-card-body -->
	</div>
</div>
<!-- /.login-box -->

<!-- Scripts -->
<script src="{{ asset('assets/admin/js/app.js') }}"></script>
<script>
	$(function() {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});
</script>
</body>
</html>